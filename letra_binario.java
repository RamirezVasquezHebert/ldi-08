
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hebert
 */
public class letra_binario {
      public static void main(String[] args) { 
                  
        String sCadena = JOptionPane.showInputDialog("Ingrese su Texto");
        System.out.println("Texto ingresado\n" + sCadena);
        System.out.println("");
        System.out.println("Caracter por caracter en ASSCI");
        for (int x=0;x<sCadena.length();x++){
         if (sCadena.codePointAt(x)==32){
                System.out.print("");
            }
         else
            System.out.print(sCadena.charAt(x) + "=" + sCadena.codePointAt(x) + " ");
            
        }
         
        System.out.println("" + "\n");
         System.out.println("Caracter por caracter en Binario");
        
        for (int x=0;x<sCadena.length();x++){
            if (sCadena.codePointAt(x)==32){
                System.out.print("");
            }else{
            String Binario = DecimalABin(Integer.toString(sCadena.codePointAt(x)));
             System.out.print(sCadena.charAt(x)+"=0"+Binario + " ");}
        }
        
        
        System.out.println();
        
     }
      
      public static String DecimalABin(String a) {
        int x = Integer.parseInt(a);
        String resultado = Integer.toString(x, 2);
        return resultado;
    }
}
